package com.amen.calculator.calculatorapplication;

import static android.support.test.espresso.Espresso.onView;
import android.support.test.espresso.ViewAction;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.filters.SmallTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

/**
 * Created by amen on 6/27/17.
 */

@RunWith(AndroidJUnit4.class)
@SmallTest
public class EspressoInterfaceTesting {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(
            MainActivity.class);

    @Test
    public void testProperInput(){
        onView(ViewMatchers.withId(R.id.editText)).check(ViewAssertions.matches(isDisplayed()));

        onView(ViewMatchers.withId(R.id.btn5)).perform(click());
        onView(ViewMatchers.withId(R.id.btn2)).perform(click());

//        onView(ViewMatchers.withId(R.id.editText)).check(ViewAssertions.matches(ViewMatchers.withText("52")));
//        onView(ViewMatchers.withText("523")).check(ViewAssertions.matches(isDisplayed()));
        onView(ViewMatchers.withText("52")).check(ViewAssertions.matches(isDisplayed()));
    }
}













