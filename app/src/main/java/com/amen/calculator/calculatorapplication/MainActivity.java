package com.amen.calculator.calculatorapplication;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.editText)
    EditText editLine;

    @BindView(R.id.btn0)
    Button btn0;
    @BindView(R.id.btn1)
    Button btn1;
    @BindView(R.id.btn2)
    Button btn2;
    @BindView(R.id.btn3)
    Button btn3;
    @BindView(R.id.btn4)
    Button btn4;
    @BindView(R.id.btn5)
    Button btn5;
    @BindView(R.id.btn6)
    Button btn6;
    @BindView(R.id.btn7)
    Button btn7;
    @BindView(R.id.btn8)
    Button btn8;
    @BindView(R.id.btn9)
    Button btn9;

    @OnClick({R.id.btn0, R.id.btn1, R.id.btn2, R.id.btn3, R.id.btn4, R.id.btn5, R.id.btn6, R.id.btn7, R.id.btn8, R.id.btn9,
            R.id.btndiv, R.id.btnminus, R.id.btnmul, R.id.btnplus})
    void onClick(View v) {
        if (v instanceof Button) {
            Button thisButton = (Button) v;
            String txt = thisButton.getText().toString();

            editLine.setText(editLine.getText() + txt);
        }
    }

    @OnClick({R.id.btneq})
    void onClickEq(View v) {
        try {
            editLine.setText(String.valueOf(Calculator.calculate(editLine.getText().toString())));
        } catch (Exception e) {

            editLine.setText("Invalid equation.");
        }
    }

    @OnClick({R.id.btnClear})
    void onClickClear(View v) {
        editLine.setText("");
    }

    @OnClick({R.id.btnRemSign})
    void onClickRemSign(View v) {
        if(!editLine.getText().toString().isEmpty()) {
            editLine.setText(editLine.getText().subSequence(0, editLine.getText().length() - 1));
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
    }
}
