package com.amen.calculator.calculatorapplication;

import junit.framework.Assert;

import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Rule;
import org.junit.Test;

/**
 * Created by amen on 2/7/17.
 */

public class CalculatorParserTests {

    private static CalculatorInstantiable calculator;

    @BeforeClass
    public static void createCalculator() {
        calculator = new CalculatorInstantiable();
    }

    @Test
    public void testParseSubFine() {
//        CalculatorInstantiable calculator = new CalculatorInstantiable();

        double result = calculator.parseAndCountSub("20-30");
        double result1 = calculator.parseAndCountSub("21235-30");
        double result2 = calculator.parseAndCountSub("39-30");
        double result3 = calculator.parseAndCountSub("0-0");

        Assert.assertEquals(-10.0, result);
        Assert.assertEquals(21205.0, result1);
        Assert.assertEquals(9.0, result2);
        Assert.assertEquals(0.0, result3);
    }

    @Test
    public void testParseSubWithSpace() {
//        CalculatorInstantiable calculator = new CalculatorInstantiable();

        double result = calculator.parseAndCountSub("20-   30");
        double result1 = calculator.parseAndCountSub("21235   -30");
        double result2 = calculator.parseAndCountSub("39-30 ");
        double result3 = calculator.parseAndCountSub(" 0-0");

        Assert.assertEquals(-10.0, result);
        Assert.assertEquals(21205.0, result1);
        Assert.assertEquals(9.0, result2);
        Assert.assertEquals(0.0, result3);
    }

    @Test
    public void testParseSubWithTwoSubsAndEmptyStrings() {
//        CalculatorInstantiable calculator = new CalculatorInstantiable();

        double result = calculator.parseAndCountSub("20- 0 - 30");
        Assert.assertEquals(20.0, result);

        double result2 = calculator.parseAndCountSub("39-30 ");
        Assert.assertEquals(9.0, result2);

        Double result3 = null;
        try {
            result3 = calculator.parseAndCountSub("- 0-0");
        } catch (NumberFormatException nfe) {
        }
        Assert.assertNull(result3);

        Double result4 = null;
        try {
            result4 = calculator.parseAndCountSub("21235 -  -30");
        } catch (NumberFormatException nfe) {
        }
        Assert.assertNull(result4);
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testParseSubNoSubX() {
        calculator.parseAndCountSub("2030");
    }

    @Test(expected = ArrayIndexOutOfBoundsException.class)
    public void testParseSubNoSub() throws ArrayIndexOutOfBoundsException{
//        CalculatorInstantiable calculator = new CalculatorInstantiable();

        Double result = null;
        try {
            result = calculator.parseAndCountSub("2030");
        } catch (ArrayIndexOutOfBoundsException nfe) {
        }
        Assert.assertNull(result);

        try {
            result = calculator.parseAndCountSub("20 30");
        } catch (NumberFormatException nfe) {
        }
        Assert.assertNull(result);
    }


}
